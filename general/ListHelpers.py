def chunk( lst, sz: int ):
    '''
    chunk() method.
    Purpose:
        Splits up a list into chunks of a specified size.
    
    Params:
        lst: An iterable object.
        sz:  An integer representing the number of elements per chunk.
    
    Returns:
        A generator object that yields the desired chunks.
    '''
    for i in range( 0, len( lst ), sz ):
        yield lst[i:i + sz]

from JSONSerializable import JSONSerializable

class Point( JSONSerializable ):
    def __init__( self, x = 0, y = 0 ):
        self.x = x
        self.y = y
    
    def __str__( self ):
        return f'Point( {self.x}, {self.y} )'

def TestJSONSerialization():
    print( 'Testing JSON serialization via JSONSerializable class...\n' )

    p = Point()
    z = Point( 2, 3 )

    print( f'p = {p}' )
    print( f'z = {z}' )

    p_json = p.to_json()
    z_json = z.to_json()

    print( f'p_json =\n{p_json}' )
    print( f'z_json =\n{z_json}' )

    v = Point.from_json( p_json )
    b = Point.from_json( z_json )

    print( f'v = {v}' )
    print( f'b = {b}' )

if __name__ == "__main__":
    TestJSONSerialization()
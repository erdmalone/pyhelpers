class Test:
    def __init__( self ):
        self.x = 0
        self.s = 'o'

def ReconstructDefinition( obj ):
    name = obj.__class__.__name__
    print( f'class {name}:' )
    print( '  def __init__( self ):' )
    for k, v in obj.__dict__.items():
        if ( type( v ) is str ):
            print( f'    self.{k} = \'\'' )
        else:
            print( f'    self.{k} = {type( v )()}' )

def CreateDefFromDict( dct ):
    print( 'class NewObject:' )
    print( '  def __init__( self ):' )
    for k, v in dct.items():
        if ( type( v ) is str ):
            print( f'    self.{k} = \'\'' )
        else:
            print( f'    self.{k} = {type( v )()}' )

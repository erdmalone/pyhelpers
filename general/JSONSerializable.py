import json

class JSONSerializable:
    '''
    JSONSerializable class.
    A mix-in class that allows an object to be easily serialized
      and deserialized to/from JSON.
    '''
    def to_dict( self ):
        '''
        to_dict() method.
        Purpose:
          Transforms the object into a standard dictionary for
            serialization.
        
        Params:
          None.

        Returns:
          A standard dictionary representing the instance data
            members of the object and their current values.
        '''
        return self._traverse_dict( self.__dict__ )
    
    def _traverse_dict( self, inst_dict : dict ):
        '''
        _traverse_dict() method.
        Purpose:
          Iterates through items in an instance dictionary,
            converting them into a format that can be readily
            serialized.
        
        Params:
          inst_dict: A dictionary of an instance of an object
            that inherits from this class.
        
        Returns:
          A dictionary with all necessary instance data to
            serialize an instance of an object that inherits
            from this class.
        '''
        output = {}
        for key, value in inst_dict.items():
            output[key] = self._traverse( key, value )
        return output
    
    def _traverse( self, key, value ):
        '''
        _traverse() method.
        Purpose:
          Traverses through a single key/value pair, including
            nested dictionaries and lists.
        
        Params:
          key: A value representing the current key being used
            for traversal.
        
          value: The value being traversed.
        
        Returns:
          This method is recursive.  Eventually returns a value
            once it reaches an object that isn't a dict, list,
            or one that doesn't have a __dict__ member.
        '''
        if ( isinstance( value, dict ) ):
            return self._traverse_dict( value )
        elif ( isinstance( value, list ) ):
            return [self._traverse( key, i ) for i in value]
        elif ( hasattr( value, '__dict__' ) ):
            return self._traverse_dict( value.__dict__ )
        else:
            return value
    
    @classmethod
    def from_json( cls, data ):
        '''
        from_json() class method.
        Purpose:
          Reconstructs an object from JSON data.
        
        Params:
          data: JSON data in a form that can be read by the
            Python JSON module.

        Returns:
          A reconstructed object based on the provided JSON data.
        '''
        kwargs = json.loads( data )
        return cls( **kwargs )
    
    def to_json( self ):
        '''
        to_json() method.
        Purpose:
          Serializes and object to the JSON format.
        
        Params:
          None.

        Returns:
          A string of JSON data with indent level 2 and with keys
            sorted for easy external reference.
        '''
        return json.dumps( self.to_dict(),
                           indent    = 2,
                           sort_keys = True )

class TestObj( JSONSerializable ):
    def __init__( self, x = 10, y = 12 ):
        self.x = x
        self.y = y
    
    def __str__( self ):
        return 'TestObj( %s, %s )' % ( self.x, self.y )

def test():
    t_obj = TestObj()
    z_obj = TestObj( 0, 13 )
    
    print( 't_obj = %s' % t_obj )
    print( 'z_obj = %s' % z_obj )
    
    t_json = t_obj.to_json()
    z_json = z_obj.to_json()
    
    print( 't_json = \n%s' % t_json )
    print( 'z_json = \n%s' % z_json )
    
    a_obj = TestObj.from_json( t_json )
    b_obj = TestObj.from_json( z_json )
    
    print( 'a_obj = %s' % a_obj )
    print( 'b_obj = %s' % b_obj )

if __name__ == '__main__':
    test()
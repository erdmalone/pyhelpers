import os, secrets

def shred( fname: str ):
    '''
    shred( fname ) function.
    Purpose:
        Fills a file with securely-generated random garbage data to prevent
        recovery.
    
    Params:
        fname: A string pointing to the path of the file to shred.
    
    Returns:
        None.
    '''
	if ( os.path.exists( fname ) ):
		data = []
		with open( fname, 'rb' ) as f:
			print( f'Reading {fname}...' )
			data = f.read()
		print( 'Shredding...' )
		data = secrets.token_bytes( len( data ) )
		with open( fname, 'wb' ) as f:
			print( f'Writing shredded data to {fname}...' )
			f.write( data )
		print( 'Done!' )
	else:
		print( f'{fname} not found, aborting...' )

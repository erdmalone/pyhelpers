from Random import AmbientRNG, SystemRNG

def TestARNG( rate = 44100,
              time = 0.05 ):
    print( 'Testing AmbientRNG class...\n' )

    rng    = AmbientRNG( rate, time )

    print( f'Pulling {rng.frames} frames per call.' )

    print( 'Testing rand_int( 1, 20 )...')
    ints = [rng.rand_int( 1, 20 ) for _ in range( 20 )]
    print( ints )

    print( '\nTesting rand_float( 1.0, 20.0 )...' )
    floats = [rng.rand_float( 1.0, 20.0 ) for _ in range( 20 )]
    print( floats )

    print( '\nTesting rand_int_generator( 1, 20, 10 )...' )
    print( list( rng.rand_int_generator( 1, 20, 10 ) ) )

    print( '\nTesting rand_float_generator( 1.0, 20.0, 10 )...' )
    print( list( rng.rand_float_generator( 1.0, 20.0, 10 ) ) )

def TestSysRNG():
    print( 'Testing SystemRNG class...\n' )
    rng = SystemRNG()

    print( 'Testing rand_int( 1, 20 )...' )
    ints = [rng.rand_int( 1, 20 ) for _ in range( 20 )]
    print( ints )

    print( '\nTesting rand_float( 1.0, 20.0 )...' )
    floats = [rng.rand_float( 1.0, 20.0 ) for _ in range( 20 )]
    print( floats )

    print( '\nTesting rand_int_generator( 1, 20, 10 )...' )
    print( list( rng.rand_int_generator( 1, 20, 10 ) ) )

    print( '\nTesting rand_float_generator( 1.0, 20.0, 10 )...' )
    print( list( rng.rand_float_generator( 1.0, 20.0, 10 ) ) )

if __name__ == '__main__':
    TestARNG( rate = 256000, time = 0.005 )
    TestSysRNG()
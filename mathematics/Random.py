import pyaudio, psutil

from Statistical import rms

class AmbientRNG:
    '''
    AmbientRNG class.
    Encapsulates methods that generate random numbers
      based on the ambient noise coming through a
      connected recording device.
    '''
    def __init__( self,
                  rate = 44100,
                  time = 0.05 ):
        self.rate   = rate
        self.time   = time
        self.frames = int( rate * time )
        self.audio  = pyaudio.PyAudio()
        self.stream = self._open_mic_stream()
    
    def _open_mic_stream( self ):
        '''
        _open_mic_stream() method.
        Purpose:
          Opens up a PyAudio stream from the first audio input device
            found on the local machine.

        Params:
          None.

        Returns:
          A PyAudio stream from which audio data can be read.
        '''
        dev_idx = self._find_input_device()
        stream  = self.audio.open( format             = pyaudio.paFloat32,
                                   channels           = 2,
                                   rate               = self.rate,
                                   input              = True,
                                   input_device_index = dev_idx,
                                   frames_per_buffer  = self.frames )

        return stream

    def _find_input_device( self ):
        '''
        _find_input_device() method.
        Purpose:
          Finds the first audio input (eg. microphone) on the local
            machine.
        
        Params:
          None.

        Returns:
          An integer representing the input device's index that can
            be used to set up a PyAudio stream.
        '''
        for i in range( self.audio.get_device_count() ):
            info = self.audio.get_device_info_by_index( i )
            for kwd in ['mic', 'input']:
                if ( kwd in info['name'].lower() ):
                    return i
        print( 'No preferred input device found, using default.' )
        return None
    
    def _listen( self ):
        '''
        _listen() method.
        Purpose:
          Listens to the input device for a certain number of audio
            frames before returing the root mean squares of the
            streamed audio.
        
        Params:
          None.

        Returns:
          A float representing the root mean squares of the audio
            frames that were streamed from the input device.
        '''
        block = None
        try:
            block = self.stream.read( self.frames,
                                      exception_on_overflow = False )
        except IOError as e:
            print( str( e ) )
            return
        return rms( block )
    
    def rand_float( self,
                    low = 0.0,
                    hi  = 1.0 ):
        '''
        rand_float() method.
        Purpose:
          Generates a random floating-point number between low and hi,
            inclusive.
        
        Params:
          low: A numerical value representing the low end of the range
            to generate.  Default 0.0.
        
          hi: A numerical value representing the high end of the range
            to generate.  Default 1.0.

        Returns:
          A true random float between low and hi, inclusive.
        '''
        val = self._listen()
        return ( val % ( hi - low ) ) + low
    
    def rand_int( self,
                  low = 0,
                  hi  = 1 ):
        '''
        rand_int() method.
        Purpose:
          Generates a random integer between low and hi, inclusive.

        Params:
          low: A numerical value representing the low end of the range
            to generate.  Default 0.
        
          hi: A numerical value representing the high end of the range
            to generate.  Default 1.

        Returns:
          A true random integer between low and hi, inclusive.
        '''
        val = self._listen()
        return int( ( val % ( hi - low + 1 ) ) + low )
    
    def rand_float_generator( self,
                         low     = 0.0,
                         hi      = 1.0,
                         n : int = 10 ):
        '''
        rand_float_generator() method.
        Purpose:
          Creates a generator of random floats between low and hi,
            inclusive.
        
        Params:
          low: A numerical value representing the low end of the range
            to generate.  Default 0.0.
        
          hi: A numerical value representing the high end of the range
            to generate.  Default 1.0.
        
          n: An integer value representing the number of elements to
            generate.  Default 10.

        Returns:
          A generator of true random floats of n elements between low
            and hi, inclusive.
        '''
        for _ in range( n ):
            val = self._listen()
            yield ( val % ( hi - low ) ) + low

    def rand_int_generator( self,
                            low     = 0,
                            hi      = 1,
                            n : int = 10 ):
        '''
        rand_float_generator() method.
        Purpose:
          Creates a generator of random integers between low and hi,
            inclusive.
        
        Params:
          low: A numerical value representing the low end of the range
            to generate.  Default 0.
        
          hi: A numerical value representing the high end of the range
            to generate.  Default 1.
        
          n: An integer value representing the number of elements to
            generate.  Default 10.

        Returns:
          A generator of true random integers of n elements between low
            and hi, inclusive.
        '''
        for _ in range( n ):
            val = self._listen()
            yield int( ( val % ( hi - low ) + 1 ) + low )

    def do_with_chance( self,
                        prob    : float,
                        percent : bool = False ):
      '''
      do_with_chance() method.
      Purpose:
        Returns True with a certain probability.
  
      Params:
        prob: A floating-point number representing the
          probability of True being returned.  Should
          either be from 0 to 1, or 0 to 100, inclusive.
  
        percent: A boolean which determines if the number
          is to be treated as a percentage chance (0-100)
          or probability (0-1).  Default False (probability).
  
      Returns:
        True or False, depending on chance.
      '''
      if ( prob <= 0.0 ):
          return False
      if ( prob >= 100.0 ):
          return True
      if ( percent or prob > 1.0 ):
          return self.do_with_chance( prob / 100.0, False )
      else:
          if ( prob >= 1.0 ):
            return True
          return self.rand_float() <= prob

class SystemRNG:
    '''
    SystemRNG class.
    Encapsulates methods that generate random numbers
      based on several continuously variable system
      statistics.
    '''
    def __init__( self ):
        pass

    def _get_values( self ):
        '''
        _get_values() method.
        Purpose:
          Uses the psutil package to get a host of system
            statistics, including CPU usage, VRAM stats,
            and disk IO.

        Params:
          None.

        Returns:
          A tuple containing CPU, VRAM, and disk IO statistics.
        '''
        cpu  = psutil.cpu_percent()
        
        _,      vavail,   vperc,  vused,  vfree        = psutil.virtual_memory()
        utimes, systimes, itimes, ntimes, dpc          = psutil.cpu_times()
        rcount, wcount,   rbytes, wbytes, rtime, wtime = psutil.disk_io_counters()

        return ( cpu,    vavail,   vperc,  vused,  vfree,
                 utimes, systimes, itimes, ntimes, dpc,
                 rcount, wcount,   rbytes, wbytes, rtime, wtime )

    def _get_seed( self ):
        '''
        _get_seed() method.
        Purpose:
          Gets the root mean squares of the values retrieved by
            _get_values() to derive a random value for the generator.

        Params:
          None.

        Returns:
          A floating-point number representing the random
            value derived from available system statistics.
        '''
        return rms( self._get_values() )

    def rand_float( self, low = 0.0, hi = 1.0 ):
        '''
        rand_float() method.
        Purpose:
          Generates a random floating-point number between low and hi,
            inclusive.

        Params:
          low: A numerical value representing the low end of the range
            to generate.  Default 0.0.

          hi: A numerical value representing the high end of the range
            to generate.  Default 1.0.

        Returns:
          A reliably random float between low and hi, inclusive.
        '''
        val = self._get_seed()
        return ( val % ( hi - low ) ) + low

    def rand_int( self, low = 0, hi = 1 ):
        '''
        rand_int() method.
        Purpose:
          Generates a random integer between low and hi, inclusive.

        Params:
          low: A numerical value representing the low end of the range
            to generate.  Default 0.
        
          hi: A numerical value representing the high end of the range
            to generate.  Default 1.

        Returns:
          A reliably random integer between low and hi, inclusive.
        '''
        val = self._get_seed()
        return int( ( val % ( hi - low + 1 ) + low ) )

    def rand_float_generator( self,
                              low     = 0.0,
                              hi      = 1.0,
                              n : int = 10 ):
        '''
        rand_float_generator() method.
        Purpose:
          Creates a generator of random floats between low and hi,
            inclusive.
        
        Params:
          low: A numerical value representing the low end of the range
            to generate.  Default 0.0.
        
          hi: A numerical value representing the high end of the range
            to generate.  Default 1.0.
        
          n: An integer value representing the number of elements to
            generate.  Default 10.

        Returns:
          A generator of reliably random floats of n elements between low
            and hi, inclusive.
        '''
        for _ in range( n ):
            yield self.rand_float( low, hi )

    def rand_int_generator( self,
                            low     = 0,
                            hi      = 1,
                            n : int = 10 ):
        '''
        rand_float_generator() method.
        Purpose:
          Creates a generator of random integers between low and hi,
            inclusive.
        
        Params:
          low: A numerical value representing the low end of the range
            to generate.  Default 0.
        
          hi: A numerical value representing the high end of the range
            to generate.  Default 1.
        
          n: An integer value representing the number of elements to
            generate.  Default 10.

        Returns:
          A generator of reliably random integers of n elements between low
            and hi, inclusive.
        '''
        for _ in range( n ):
            yield self.rand_int( low, hi )

    def do_with_chance( self,
                        prob    : float,
                        percent : bool = False ):
      '''
      do_with_chance() method.
      Purpose:
        Returns True with a certain probability.
  
      Params:
        prob: A floating-point number representing the
          probability of True being returned.  Should
          either be from 0 to 1, or 0 to 100, inclusive.
  
        percent: A boolean which determines if the number
          is to be treated as a percentage chance (0-100)
          or probability (0-1).  Default False (probability).
  
      Returns:
        True or False, depending on chance.
      '''
      if ( prob <= 0.0 ):
          return False
      if ( prob >= 100.0 ):
          return True
      if ( percent or prob > 1.0 ):
          return self.do_with_chance( prob / 100.0, False )
      else:
          if ( prob >= 1.0 ):
            return True
          return self.rand_float() <= prob

import math, random
import statistics as stats

def rms( lst : list ):
    '''
    rms() method.
    Purpose:
      Computes the root mean squares of a list of inputs.

    Params:
      lst: A list of numerical values.

    Returns:
      A floating-point number representing the square root
        of the mean (average) of the squares of the input
        values.
    '''
    return math.sqrt( stats.mean( [x ** 2 for x in lst] ) )

def do_with_chance( prob    : float,
                    percent : bool = False ):
    '''
    do_with_chance() method.
    Purpose:
      Returns True with a certain probability.  Uses
        Python's native random library.
  
    Params:
      prob: A floating-point number representing the
        probability of True being returned.  Should
        either be from 0 to 1, or 0 to 100, inclusive.
  
      percent: A boolean which determines if the number
        is to be treated as a percentage chance (0-100)
        or probability (0-1).  Default False (probability).
  
    Returns:
      True or False, depending on chance.
    '''
    if ( prob <= 0.0 ):
        return False
    if ( prob >= 100.0 ):
        return True
    if ( percent or prob > 1.0 ):
        return do_with_chance( prob / 100.0, False )
    else:
        if ( prob >= 1.0 ):
          return True
        return random.random() <= prob
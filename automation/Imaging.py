import cv2, time

import numpy     as np
import pyautogui as pyag

def centre_search( img       : str,
                   precision : float = 0.67 ):
    '''
    centre_search() method.
    Purpose:
      Searches the screen for a given image, with a degree of accuracy,
        returning the X, Y coordinate of the centre of said image, or
        ( -1, -1 ) if one isn't found with the provided level of
        precision.

    Params:
      img: A string representing the path to the image to be found.

      precision: A float representing the degree of accuracy desired
        when searching for the image.  Default 0.67 (67%) accuracy.

    Returns:
      A two-tuple of coordinates that point to the centre of the image,
        or ( -1, -1 ) if the image wasn't found to within the desired
        level of accuracy.
    '''
    im       = pyag.screenshot()
    im_gray  = cv2.cvtColor( np.array( im ), cv2.COLOR_BGR2GRAY )
    template = cv2.imread( img, 0 )
    
    template.shape[::-1]
    
    res = cv2.matchTemplate( im_gray,
                             template,
                             cv2.TM_CCOEFF_NORMED )
    _, max_val, _, max_loc = cv2.minMaxLoc( res )
    if ( max_val < precision ):
        return ( -1, -1 )
    height, width = template.shape
    return ( max_loc[0] + ( width  // 2 ),
             max_loc[1] + ( height // 2 ) )

def centre_search_loop( img       : str,
                        precision : float = 0.67,
                        rate      : float = 0.1,
                        timeout   : float = 10.0 ):
    '''
    centre_search_loop() method.
    Purpose:
      Performs the centre_search() to find an image, within a certain
        degree of accuracy, continuously until found.

    Params:
      img: A string representing the path to the image to be found.

      precision: A float representing the degree of accuracy desired
        when searching for the image.  Default 0.67 (67%) accuracy.

      rate: A float representing the rate at which attempts are made
        to detect the provided image.  Default 0.1 seconds.

      timeout: A float representing the maximum amount of time this
        method has to find the image before failing.  Default 10.0
        seconds.

    Returns:
      A two-tuple of coordinates that point to the centre of the image,
        or ( -1, -1 ) if the image wasn't found to within the desired
        level of accuracy before timeout occurs.
    '''
    t    = 0.0
    x, y = centre_search( img, precision )
    while ( x == -1 ):
        if ( t >= timeout ):
            return ( -1, -1 )
        else:
            t += rate
            time.sleep( rate )
            x, y = centre_search( img, precision )
    return ( x, y )
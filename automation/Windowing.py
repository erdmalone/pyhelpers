import pygetwindow as pygw

def focus_window_by_name( name: str ):
    '''
    focus_window_by_name() method.
    Purpose:
      Attempts to bring the first window found by the provided
        name to the foreground, using the window's title
        (eg. "Mozilla Firefox").

    Params:
      name: A string representing the name of the window to be
        brought to the foreground.
        
    Returns:
      The window that has been brought to the foreground.
    '''
    wnd = pygw.getWindowsWithTitle( name )[0]
    wnd.activate()
    return wnd